Lib9_S3_trimmed_R1_bismark_bt2_pe.deduplicated.bam

Parameters used to extract methylation information:
Bismark Extractor Version: v0.22.3
Bismark result file: paired-end (SAM format)
Output specified: comprehensive
No overlapping methylation calls specified


Processed 73993 lines in total
Total number of methylation call strings processed: 147986

Final Cytosine Methylation Report
=================================
Total number of C's analysed:	1276020

Total methylated C's in CpG context:	51257
Total methylated C's in CHG context:	59135
Total methylated C's in CHH context:	359659

Total C to T conversions in CpG context:	155633
Total C to T conversions in CHG context:	163001
Total C to T conversions in CHH context:	487335

C methylated in CpG context:	24.8%
C methylated in CHG context:	26.6%
C methylated in CHH context:	42.5%


