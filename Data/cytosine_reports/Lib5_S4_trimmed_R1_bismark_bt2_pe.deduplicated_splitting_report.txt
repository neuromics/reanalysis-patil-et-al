Lib5_S4_trimmed_R1_bismark_bt2_pe.deduplicated.bam

Parameters used to extract methylation information:
Bismark Extractor Version: v0.22.3
Bismark result file: paired-end (SAM format)
Output specified: comprehensive
No overlapping methylation calls specified


Processed 111656 lines in total
Total number of methylation call strings processed: 223312

Final Cytosine Methylation Report
=================================
Total number of C's analysed:	1612060

Total methylated C's in CpG context:	4142
Total methylated C's in CHG context:	4595
Total methylated C's in CHH context:	21648

Total C to T conversions in CpG context:	316028
Total C to T conversions in CHG context:	330405
Total C to T conversions in CHH context:	935242

C methylated in CpG context:	1.3%
C methylated in CHG context:	1.4%
C methylated in CHH context:	2.3%


