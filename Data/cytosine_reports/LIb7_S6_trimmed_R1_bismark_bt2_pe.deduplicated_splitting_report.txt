LIb7_S6_trimmed_R1_bismark_bt2_pe.deduplicated.bam

Parameters used to extract methylation information:
Bismark Extractor Version: v0.22.3
Bismark result file: paired-end (SAM format)
Output specified: comprehensive
No overlapping methylation calls specified


Processed 110180 lines in total
Total number of methylation call strings processed: 220360

Final Cytosine Methylation Report
=================================
Total number of C's analysed:	1935751

Total methylated C's in CpG context:	91978
Total methylated C's in CHG context:	105251
Total methylated C's in CHH context:	564576

Total C to T conversions in CpG context:	228656
Total C to T conversions in CHG context:	241259
Total C to T conversions in CHH context:	704031

C methylated in CpG context:	28.7%
C methylated in CHG context:	30.4%
C methylated in CHH context:	44.5%


