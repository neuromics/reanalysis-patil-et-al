LIb8_S7_trimmed_R1_bismark_bt2_pe.deduplicated.bam

Parameters used to extract methylation information:
Bismark Extractor Version: v0.22.3
Bismark result file: paired-end (SAM format)
Output specified: comprehensive
No overlapping methylation calls specified


Processed 59678 lines in total
Total number of methylation call strings processed: 119356

Final Cytosine Methylation Report
=================================
Total number of C's analysed:	818301

Total methylated C's in CpG context:	7937
Total methylated C's in CHG context:	8943
Total methylated C's in CHH context:	45031

Total C to T conversions in CpG context:	151233
Total C to T conversions in CHG context:	158930
Total C to T conversions in CHH context:	446227

C methylated in CpG context:	5.0%
C methylated in CHG context:	5.3%
C methylated in CHH context:	9.2%


