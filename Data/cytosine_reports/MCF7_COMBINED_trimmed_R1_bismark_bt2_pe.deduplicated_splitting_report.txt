MCF7_COMBINED.bam

Parameters used to extract methylation information:
Bismark Extractor Version: v0.22.3
Bismark result file: paired-end (SAM format)
Output specified: comprehensive
No overlapping methylation calls specified


Processed 537961 lines in total
Total number of methylation call strings processed: 1075922

Final Cytosine Methylation Report
=================================
Total number of C's analysed:	10779585

Total methylated C's in CpG context:	526108
Total methylated C's in CHG context:	607218
Total methylated C's in CHH context:	3663996

Total C to T conversions in CpG context:	1094801
Total C to T conversions in CHG context:	1138115
Total C to T conversions in CHH context:	3749347

C methylated in CpG context:	32.5%
C methylated in CHG context:	34.8%
C methylated in CHH context:	49.4%
