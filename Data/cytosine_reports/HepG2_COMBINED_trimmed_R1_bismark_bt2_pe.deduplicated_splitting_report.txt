HepG2_COMBINED.bam

Parameters used to extract methylation information:
Bismark Extractor Version: v0.22.3
Bismark result file: paired-end (SAM format)
Output specified: comprehensive
No overlapping methylation calls specified


Processed 393050 lines in total
Total number of methylation call strings processed: 786100

Final Cytosine Methylation Report
=================================
Total number of C's analysed:	9176235

Total methylated C's in CpG context:	723904
Total methylated C's in CHG context:	827397
Total methylated C's in CHH context:	4642409

Total C to T conversions in CpG context:	481293
Total C to T conversions in CHG context:	502874
Total C to T conversions in CHH context:	1998358

C methylated in CpG context:	60.1%
C methylated in CHG context:	62.2%
C methylated in CHH context:	69.9%


