<!-- PROJECT LOGO -->
<br />
<div align="center">

<h3 align="center">Reanalysis Patil et al. 2019</h3>

  <p align="center">
    Reanalisis of WGBS raw fastq files from <a href="https://doi.org/10.1093/nar/gkz762">Patil et al. 2019</a>
  </p>
</div>


## How to

We recommend to download the whole repository to your local machine, unzip it,
and open the self-contained, compiled html document
(reanalysis_patil_et_al.html).

[Download repo](https://git.app.uib.no/neuromics/reanalysis-patil-et-al/-/archive/master/reanalysis-patil-et-al-master.zip)

## About this repository

The reanalysis is all contained in a R markdown file:
[reanalysis_patil_et_al.rmd](reanalysis_patil_et_al.rmd). It is already
compiled in an HTML file and can be downloaded and visualised in a browser:
[reanalysis_patil_et_al.html](reanalysis_patil_et_al.html).

Additionally, QC results using fastQC/MultiQC for the raw fastq files can be
found in the folder [qc_1_raw_fastqfiles](qc_1_raw_fastqfiles).



