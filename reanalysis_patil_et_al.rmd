---
title: "Reanalysis of Patil _et al._ 2019"
author: "Gonzalo S. Nido"
date: "`r Sys.Date()`"
output:
    html_document:
        toc: true
        toc_float: true
        number_sections: false
        highlight: tango
---

<!--
To convert Markdown document to HTML,
R -e "rmarkdown::render('reanalysis_patil_et_al.rmd')"
-->

```{r global_options, include=FALSE, cache=FALSE}
require("knitr")
knitr::opts_chunk$set(fig.path='Figs_Rmd/', fig.height=3, fig.width=4,
                      echo=FALSE, warning=FALSE, message=FALSE,
                      cache=FALSE)
require("tidyverse")
require("pander")
require("kableExtra")
require("scales")
require("openxlsx")
require("cowplot")
require("seqinr")
require("broom")
require("purrr")
require("ggfortify")
require("mgcv")
require("pheatmap")

dir.create("./Figs", showWarnings=FALSE)
dir.create("./Tables", showWarnings=FALSE)
```

# Upstream methods

We were kindly provided with a set of raw FASTQ files form the authors of
__"Human mitochondrial DNA is extensively methylated in a non-CpG context"__
([Patil _et al._ 2019](https://doi.org/10.1093/nar/gkz762)) together with three
Word documents containing the sample mapping for the files. The Word documents
are available here: [FQ_2017-10-13.docx](./Data/FQ_2017-10-13.docx),
[FQ_2018-02-20.docx](./Data/FQ_2018-02-20.docx),
[FQ_2018-06-06.docx](./Data/FQ_2018-06-06.docx). From the filenames is evident
that the samples were sequenced in three different batches. One of the samples
provided as a FASTQ (HepaRG), however, was not present within these documents.

The following table summarizes the samples received and reanalyzed in the
present document. The columns __"date"__, __"dir"__ and __"filename"__
correspond to the directory structure of the FASTQ files obtained from the
authors, while the __"word_batch_id"__, __"word_doc_id"__ and
__"word_description"__ correspond to the IDs provided in the Word documents.

```{r print_id_mapping_table}
id_mapping <- read_csv("./Data/id_mapping.csv", show_col_types=FALSE)
kbl(id_mapping) %>% kable_paper("hover", full_width=FALSE) %>%
    row_spec(14, color="#D7261E")
```

Note that one of the FASTQ files (<tt>Lib6_S5.R1.fastq.gz</tt>) was corrupt and
data could not be extracted (red row above). This sample was, hence, not
reanalyzed.


```{r fix_id_mapping_table}
# remove gzip corrupt sample
id_mapping <- id_mapping %>% filter(filename!="Lib6_S5")
# remove lambda plasmid sample
id_mapping <- id_mapping %>% filter(filename!="Lib8_S2")
# fix missing info for HepaRG sample
id_mapping[id_mapping$filename=="HepaRG","word_description"] <- "HepaRG"
id_mapping[id_mapping$filename=="HepaRG","word_batch_id"] <- "MSEQ_3"
id_mapping[id_mapping$filename=="HepaRG","word_doc_id"] <- "LIBHEPARG"
# change id_mapping colnames
colnames(id_mapping) <- c("date", "dir", "filename", "batch", "lib", "sample_id")
# unique sample names
id_mapping[id_mapping$filename=="Lib11_S2","sample_id"] <- "MCF7_Lib11"
id_mapping[id_mapping$filename=="MCF7","sample_id"] <- "MCF7_Lib12"
id_mapping[id_mapping$filename=="Lib1_S1","sample_id"] <- "HEPG2_Lib1_S1"
id_mapping[id_mapping$filename=="Lib2_S2","sample_id"] <- "HEPG2_Lib2_S2"
# add samples with concatenated libraries
id_mapping <- id_mapping %>% 
    bind_rows(tibble(date=as.Date("2017-10-13"), dir=NA, filename="MCF7_COMBINED",
                     batch="MCF7_COMBINED", lib="LIB_11_and_LIB_12",
                     sample_id="MCF7_COMBINED")) %>%
    bind_rows(tibble(date=as.Date("2018-06-06"), dir=NA, filename="HepG2_COMBINED",
                     batch="HepG2_COMBINED", lib="LIB1_S1_and_LIB2_S2", 
                     sample_id="HepG2_COMBINED"))
id_mapping$sample_id <- factor(id_mapping$sample_id,
                               levels=c("HEPG2_BAMH1_treated", "HEPG2_Lib1_S1",
                                   "HEPG2_Lib2_S2", "HepG2_COMBINED", 
                                   "HEPATOCYTES", "MCF10A", "MCF7_Lib11",
                                   "MCF7_Lib12", "MCF7_COMBINED", "HepaRG", 
                                   "HEPARG_D0", "HMEC", "NORMAL_LIVER",
                                   "PRIMARY_TUMOUR_LIVER_HCC", "MCF10A_DNMT3A_KD_siRNA",
                                   "MCF10A_DNMT3B_KD_siRNA"))
id_mapping <- id_mapping %>% arrange(sample_id)
```

FASTQ files were first aligned to establish the trimming parameters by
visualising the resulting M-plots. Alignments were always carried out using
Bismark v0.22.3. Subsequently, FASTQ files were trimmed using Trimmomatic v0.39
with the following settings:

* <tt>ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 </tt>
* <tt>CROP:75</tt>
* <tt>HEADCROP:7</tt>
* <tt>LEADING:3</tt>
* <tt>TRAILING:3</tt>
* <tt>SLIDINGWINDOW:4:15</tt>
* <tt>MINLEN:15</tt>

Results from M-bias estimation (data and plots) are available in the Appendix
of this document.

Trimming was particularly important for some samples, which showed widespread
stretches of N basecalls.

Following trimming, reads were aligned against the rCRS reference,
deduplicated, and methylation extracted using the Bismark v0.22.3 suite.
Samples were also aligned against the lambda phage reference [GenBank
J02459.1](https://www.ncbi.nlm.nih.gov/nuccore/215104)
to test for the presence of spike ins in the samples.

The sample with unmethylated lambda was aligned only against the lambda
reference.




# Lambda sample

The authors use an unmethylated lambda sample to estimate a false positive.
They subsequently filter positions based on depth of coverage and methylation
to calculate an _accuracy_ (ACC).

In practical terms, the authors define each reference position as either
_methylated_ or _unmethylated_ depending on a combination of unconversion
rate and depth of coverage. Specifically, a position is defined as
_methylated_ if it exhibits an unconversion rate above 9% and a depth of
coverage above 10x. The position is considered _unmethylated_ otherwise
(positions are never discarded from the analysis).

The reasoning behind this "noise filtering" is as follows (from the Methods
section):

    "The amount of potential incorrect methylation calls was assessed from these
  data to determine the threshold of sequencing reads that need to be
  considered for avoiding potential false positive methylation calls. The
  following formula was used:

$$ \text{False Positive Rate} = \left[\left(\frac{\text{#methylated cytosines in plasmid genome}}{\text{total # cytosines in plasmid genome}}\right)*100\right]$$

    From this analysis, we found that including data points<sup>1</sup> under the
  threshold of 10 reads and all cytosines with methylation levels under 9%
  considerably increased the false positive detection of methylation,
  decreasing the accuracy to <97%. Therefore, __for all datasets, all sites with
  coverage of <10 reads as well as all sites with methylation levels under 9%
  were removed.  This ensured that the methylation calls from the data are at
  least 98.95% accurate.__"

<sup>1</sup> data points refer to positions

They also comment on this choice in the Results section:

    "To ensure quality methylation calls, 100% unmethylated plasmid DNA was
  processed and sequenced simultaneously alongside the mtDNA samples (Figure
  1F). When all methylation calls with <10x coverage and <9% methylation were
  excluded, 378 cytosines remained false positive out of a potential 24182
  cytosines. Hence 98.44% accuracy in methylation calls can be achieved with
  minimum 10x coverage and a detection threshold of 9% methylation. All
  reads<sup>2</sup> not matching this threshold criteria were excluded as
  background noise."

<sup>2</sup> this is a typo, here the authors do not mean "reads" but rather
"positions"

This cannot be considered a measure of accuracy. In fact, using the sequencing
data from their lambda sample and by following this logic, a threshold of 10x and
50% methylation would result in 100% accuracy.

The authors, hence, choose to disregard the high-resolution methylation
information in favor of a binary representation of methylation for a given
cytosine position.

In other words: their measure of methylation for a particular position is __"a
base is _methylated_ if we can detect anything above 9% and 10x; it is
_unmethylated_ otherwise"__.

The following table represents different values of the accuracy expected with
different choices of parameters as per the original formula in the Patil _et
al_. publication using the original lambda sample data.

```{r lambda_sample}
# read meth data
lambda_meth <- read_tsv("./Data/cytosine_reports/Lib8_S2_lambda.cytosine.CX_report.txt",
                   col_types="fifiifc",
                   col_names=c("chr", "pos", "strand", "methylated", 
                               "unmethylated", "context", "triplet")) %>%
        mutate(context2=as.factor(ifelse(context=="CG", "CpG", "non-CpG")),
               context3=as.factor(substr(triplet, 1, 2)))
lambda_meth$coverage <- mapply(sum, lambda_meth$methylated, 
                               lambda_meth$unmethylated, na.rm=TRUE)
lambda_meth$beta <- mapply("/", lambda_meth$methylated, 
                           lambda_meth$coverage)
lambda_meth$patil_pass <- ifelse(lambda_meth$coverage >=10 & lambda_meth$beta >= 0.09,
                                 TRUE, FALSE)
lambda_meth$sample_id <- factor("lambda")

# read lambda sequence
lambda_dna <- as.character(read.fasta("./Data/lambda.fasta", as.string=TRUE)[[1]])
# count number of cytosines in both strands
tot_lambda_cs <- str_count(lambda_dna, "c") + str_count(lambda_dna, "g")

#lambda_meth %>% 
#    ggplot() + geom_line(aes(x=pos, y=beta, colour=strand))

patil_accuracy <- tibble(min_meth=c(0.09, 0.09, 0.25, 0.25, 0.50, 0.50), 
                         min_depth=c(1,10,1,10,1,10))

patil_accuracy$ACC <- apply(patil_accuracy, 1, function(thresholds) {
    min_meth=thresholds[1]
    min_depth=thresholds[2]
    #message(min_meth)
    #message(min_depth)
    Table <- lambda_meth %>% mutate(filt_pass=ifelse((beta >= min_meth) & (coverage >= min_depth), "FP", "TP")) %>%
        pull(filt_pass) %>% table %>% prop.table
    Table["TP"]
})

patil_accuracy %>%
    mutate(min_meth=paste0(min_meth*100, '%'),
           min_depth=paste0(formatC(min_depth), 'x'),
           ACC_percent=paste0(round(ACC*100, digits=2), '%')) %>%
    select(-ACC) %>%
kbl() %>% kable_paper("hover", full_width=FALSE)
```

__NOTE:__ values reported in the previous table for the 9% and 10x parameters
(second row) are not identical to those provided in the original publication
due to differences in the alignments and aligment post-processing.


## Nucleotide frequencies

Using the bam2nuc script from the Bismark suite, we quantified the coverage
bias between the different bases and dinucleotides. The bias corresponds to how
the observed base composition deviates from what it would be expected from a
uniformly covered mtDNA (ratio between observed and expected base composion).

The following table shows the coverage biases for each nucleotide (observed
nucleotide coverage / expected nucleotide coverage) demonstrating a marked bias
in all samples analyzed. These biases are expected from heat denaturation BS
library preparations, especially when a fragmentation stage precedes BS
reaction. C-rich regions are degraded, in particular if unmethylated. This was
demonstrated in [Olova _et al._ 2018](https://doi.org/10.1186/s13059-018-1408-2), an
article published prior to the dataset reanalyzed here.

```{r nucleotide_frequencies}
nucBias <- lapply(id_mapping$filename, function(f) {
    Data <- read_tsv(paste0("./Data/bam2nuc_reports/", f, 
                            '_trimmed_R1_bismark_bt2_pe.deduplicated.nucleotide_stats.txt'),
                     skip=1, col_names=c("nuc", "count_observed", "percent_observed",
                                         "count_expected", "percent_expected", "coverage"),
                     col_types="fididd")
    sid <- which(id_mapping$filename == f)[1]
    Data$sample_id <- id_mapping$sample_id[sid]
    nuc_classes <- rev(c("A", "T", "C", "G",
                     "AC", "CA", "TC", "CT",
                     "CC", "CG", "GC", "GG",
                     "AG", "GA", "TG", "GT",
                     "TT", "TA", "AT", "AA"))
    Data$nuc <- factor(Data$nuc, levels=nuc_classes)
    Data
}) %>% do.call("bind_rows", .)

biasTable <- nucBias %>% filter(nuc %in% c("A", "C", "G", "T")) %>%
    select(sample_id, nuc, percent_observed, percent_expected) %>%
    mutate(bias=percent_observed/percent_expected) %>%
    select(sample_id, nuc, bias) %>%
    pivot_wider(names_from="nuc", values_from="bias", names_prefix="bias_")

kbl(biasTable, digits=2) %>% kable_paper("hover", full_width=FALSE) %>%
    column_spec(2, bold=TRUE, color=ifelse(biasTable$bias_A>1, "darkred", "darkblue")) %>%
    column_spec(3, bold=TRUE, color=ifelse(biasTable$bias_C>1, "darkred", "darkblue")) %>%
    column_spec(4, bold=TRUE, color=ifelse(biasTable$bias_G>1, "darkred", "darkblue")) %>%
    column_spec(5, bold=TRUE, color=ifelse(biasTable$bias_T>1, "darkred", "darkblue"))
#biasTable %>% write.xlsx("./Tables/base_coverage_bias.xlsx")
```

This bias against C positions is further exacerbated with PCR amplification,
which we can assess by looking at the same statistics for the non deduplicated
alignments.

__NOTE:__ some samples had more than one library prepared and sequenced. We
chose to analyze these libraries both separately and combined for the sake of
completeness. Samples corresponding to the combined libraries have a
<tt>COMBINED</tt> postfix in their sample name (e.g., <tt>MCF7_Lib11</tt>,
<tt>MCF7_Lib12</tt>, <tt>MCF7_COMBINED</tt>.

```{r bias_post_dedup}
#non dedup:
nucBias_nonDedup <- lapply(id_mapping$filename, function(f) {
    if (f == "MCF7_COMBINED") {return(NULL)}
    if (f == "HepG2_COMBINED") {return(NULL)}
    Data <- read_tsv(paste0("./Data/bam2nuc_reports_NON_DEDUP/", f, 
                            '_trimmed_R1_bismark_bt2_pe.nucleotide_stats.txt'),
                     skip=1, col_names=c("nuc", "count_observed", "percent_observed",
                                         "count_expected", "percent_expected", "coverage"),
                     col_types="fididd")
    sid <- which(id_mapping$filename == f)[1]
    Data$sample_id <- id_mapping$sample_id[sid]
    nuc_classes <- rev(c("A", "T", "C", "G",
                     "AC", "CA", "TC", "CT",
                     "CC", "CG", "GC", "GG",
                     "AG", "GA", "TG", "GT",
                     "TT", "TA", "AT", "AA"))
    Data$nuc <- factor(Data$nuc, levels=nuc_classes)
    Data
}) %>% do.call("bind_rows", .)

biasTable2 <- bind_rows(
    nucBias %>% filter(nuc == "C") %>%
        select(sample_id, nuc, percent_observed, percent_expected) %>%
        mutate(bias=percent_observed/percent_expected) %>%
        select(sample_id, nuc, bias) %>%
        mutate(deduplicated="dedup"),
    nucBias_nonDedup %>% filter(nuc == "C") %>%
        select(sample_id, nuc, percent_observed, percent_expected) %>%
        mutate(bias=percent_observed/percent_expected) %>%
        select(sample_id, nuc, bias) %>%
        mutate(deduplicated="not_dedup")) %>%
    pivot_wider(names_from=c("nuc", "deduplicated"), values_from="bias",
                names_prefix="bias_") %>% 
    select(bias_C_not_dedup, bias_C_dedup) %>%
    mutate(dedup_ameliorates_bias=ifelse(bias_C_dedup>bias_C_not_dedup, "TRUE", "FALSE")) %>%
    na.omit
kbl(biasTable2, digits=3) %>%
    kable_paper("hover", full_width=FALSE) %>%
    column_spec(3, bold=TRUE, color="white",
                background=ifelse((biasTable2$bias_C_dedup-biasTable2$bias_C_not_dedup)>0,
                                  "#D38D7E", "#43A0B8"))
```

With the exception of 2 samples, deduplication consistently ameliorates the
bias (absence of bias correspond to 1).


## Distribution of per-read methylation proportions

As mentioned before, the tendency to deplete the DNA preparation from C-rich
regions is even more so for non-methylated C-rich regions, effectively
resulting in overestimating per-sample methylation levels. In order to test
this accepted notion ([Olova _et
al._ 2018](https://doi.org/10.1186/s13059-018-1408-2)), we can simply look at all
the aligned sequencing reads and characterize them in terms of (i) the number
of cytosines in the mtDNA reference that they overlap with, __C~tot~__, and
(ii) the proportion of those positions that are non-converted/methylated,
__P(C~meth~)__.

Reads that are aligned to larger numbers of cytosines are mapped to C-rich
regions of the mtDNA (large __C~tot~__ values), and viceversa. In the scenario
where unmethylated C-rich DNA regions are depleted from the sample preparation,
we should expect reads aligned with large __C~tot~__ values to be mostly
methylated, i.e., show also relatively large values of __P(C~meth~)__.

We can easily access this information from the alignment files generated by
Bismark, which contain the methylated and unmethylated calls for all positions
on each read in the <tt>XM:Z</tt> field.

```{r read_per_read_meth_data}
# Read per-read data
per_read_meth <- lapply(id_mapping$filename, function(f) {
    Data <- read_tsv(paste0("./Data/meth_per_read/", f, '_meth_per_read.tsv'), col_types="iiiiici") %>%
        mutate(strand=factor(ifelse(strand%in%c(147, 99), "L", "H")))
    names(Data) <- c("meth", "Cs", "mapq", "pos", "strand", "cigar", "snps")
    Data <- Data %>% mutate(prop_meth=meth/Cs)
    Data$sample_id <- id_mapping[id_mapping$filename==f,]$sample_id
    Data %>% select(sample_id, meth, Cs, prop_meth, mapq, pos, strand, snps)
}) %>% do.call("bind_rows", .)
```

```{r plot_per_read_data, fig.height=14, fig.width=10}
beta_normalize <- function(x) {
    x_ <- ((x - min(x)) / (max(x) - min(x)))
    (x_ * (length(x_) - 1) + 0.5) / length(x_)
}
# Expected C frequency in even covered mtDNA
mtdna <- read.fasta("./Data/rCRS.fasta", as.string=TRUE)[[1]] %>% as.character
read_length <- 68
C_freq <- sapply(1:(nchar(mtdna)-read_length), function(P){
                Read <- substring(mtdna, P, P+read_length)
                c(L=str_count(Read, "c"), H=str_count(Read, "g"))
            }) %>% t %>% as_tibble %>%
    mutate(prop_meth="expected") %>%
    pivot_longer(!prop_meth, names_to="strand", values_to="Cs")

# Observed (per sample)
Plots_obs <- lapply(id_mapping$sample_id, function(S) {
    per_read_meth %>% filter(sample_id==S) %>% select(Cs) %>% mutate(prop_meth="observed") %>%
        bind_rows(C_freq) %>%
        ggplot(aes(Cs, factor(prop_meth), group=factor(prop_meth))) +
        geom_bin2d(aes(fill=..density..), drop=FALSE, binwidth=c(2,1)) +
        scale_fill_viridis_c(option="magma")+
        #scale_fill_gradient(low="white", high="black") +
        coord_cartesian(xlim=c(0,40)) +
        xlab(expression(C[tot])) +
        ylab("P(5mC)") +
        theme_bw() + 
        theme(legend.position="none", panel.grid.major=element_blank(), panel.grid.minor=element_blank(),
              axis.title.y=element_text(colour="transparent"))
})
names(Plots_obs) <- id_mapping$sample_id

Plots <- lapply(id_mapping$sample_id, function(S) {
    tmp_data <- per_read_meth %>% filter(sample_id==S, Cs>0) %>%
        mutate(prop_meth=beta_normalize(prop_meth))
    gam_fit <- tmp_data %>% 
        #mutate(prop_meth=beta_normalize(prop_meth)) %>%
        mgcv::gam(formula=prop_meth ~ s(Cs, bs="cs"), data=.)
    new_fits <- tibble(Cs=seq(min(tmp_data$Cs), max(tmp_data$Cs)))
    new_fits$beta <- predict(gam_fit, newdata=new_fits)
    ggplot() +
        geom_bin2d(data=tmp_data, aes(x=Cs, y=prop_meth), binwidth=c(2,.1), drop=FALSE) +
        geom_line(data=new_fits, aes(Cs, beta), colour="white", linetype=2) +
        #scale_fill_gradient(low="white", high="black") +
        scale_fill_viridis_c(option="magma")+
        coord_cartesian(xlim=c(0,40), ylim=c(0,1)) +
        xlab(expression(C[tot])) +
        ylab("P(5mC)") +
        theme_bw() + 
        ggtitle(S) +
        theme(plot.title=element_text(size=11),
              legend.position="none", panel.grid.major=element_blank(), panel.grid.minor=element_blank(),
              axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank())
})
names(Plots) <- id_mapping$sample_id

# Output HD figures in one panel per sample
#for (S in id_mapping$sample_id) {
#    pdf(paste0("./Figs/per_read_meth_", S, ".pdf"), width=5, height=3.7)
#    plot_grid(Plots[[S]], Plots_obs[[S]], align="v", ncol=1, rel_heights=c(7,2.45)) %>% print
#    dev.off()
#}

allPlots <- c(Plots[1:4], Plots_obs[1:4],
              Plots[5:8], Plots_obs[5:8],
              Plots[9:12], Plots_obs[9:12],
              Plots[13:16], Plots_obs[13:16])

plot_grid(plotlist=allPlots, align="hv", ncol=4,
          rel_heights=c(4,2,4,2,4,2,4,2))
```

From the stripes below each plot, it is evident that the observed cytosines per
read are below what expected in a uniformly covered mtDNA. This confirms a
depletion of C-rich regions in the DNA sample.

From the plots, with the exception of two of the samples (the same exceptions
as before), it is evindent a trend by which the higher the C content in a read,
the more unconverted/methylated the entirety of the read is. DNA fragments
originating from C-rich regions are degraded if they are not methylated, and
only 5mC-rich methylated fragments remain.


## Coverage strand bias

Heavy (H) and light (L) strands of the mtDNA have different C-content
(with the H strand having significantly less cytosines). It was shown that both
genomic C-rich satellite regions and mtDNA exhibited a differential degradation
of the strands following BS conversion ([Olova _et
al._ 2018](https://doi.org/10.1186/s13059-018-1408-2)).

By using the same read-level methylation data, we can visualize the expected
proportions of cytosines in the reads and compare them with the observed
proportions, factorizing by strand.

```{r coverage_strand_bias_plots, fig.height=11, fig.width=10}
# Expected/observed coverage distributions factorized by strand
Plots_density <- lapply(id_mapping$sample_id, function(S) {
    nReads <- per_read_meth %>% filter(sample_id==S) %>% nrow
    expected <- C_freq %>% group_by(strand, Cs) %>% summarise(N=n(), .groups="drop")
    expected$prop <- expected$N/sum(expected$N)
    expected$scaled_Cs <- round(expected$prop*nReads)
    expected <- mutate(expected, ids=map(scaled_Cs, seq_len)) %>% unnest(cols=c(ids)) %>% select(Cs, strand) %>%
        mutate(condition="expected")
    
    per_read_meth %>% filter(sample_id==S) %>% select(Cs, strand) %>% mutate(condition="observed") %>%
        bind_rows(expected) %>%
        ggplot() +
        geom_density(aes(x=Cs, after_stat(count), colour=strand, linetype=condition), alpha=.2, adjust=3) +
        scale_colour_manual(values=c("red", "black")) +
        coord_cartesian(xlim=c(0, 40)) +
        xlab("Cytosines per read") +
        theme_bw() +
        theme(plot.title=element_text(size=11)) +
        ggtitle(S)
})
names(Plots_density) <- id_mapping$sample_id

# Workaround to plot with legend
Legend <- get_legend(Plots_density[[1]])
Plots_density <- lapply(Plots_density, function(P) P + theme(legend.position="none"))
Plots_density[[length(Plots_density)+1]] <- Legend

do.call("plot_grid", c(Plots_density, list(ncol=3)))
```


## Correlation depth-methylation

The following plots display each mtDNA position according to the coverage
(x-axis) and methylation proportion (y-axis). Positions are coloured according
to their strand (red, +; blue, -).

```{r}
# Read Cytosine files
meth <- lapply(id_mapping$filename, function(f) {
    Data <- read_tsv(paste0("./Data/cytosine_reports/", f, '_trimmed_R1_bismark_bt2_pe.deduplicated.cytosine.CX_report.txt'),
                     col_types="fifiifc",
                     col_names=c("chr", "pos", "strand", "methylated", "unmethylated", "context", "triplet")) %>%
        mutate(context2=as.factor(ifelse(context=="CG", "CpG", "non-CpG")),
               context3=as.factor(substr(triplet, 1, 2)))
    Data$coverage <- mapply(sum, Data$methylated, Data$unmethylated, na.rm=TRUE)
    Data$beta <- mapply("/", Data$methylated, Data$coverage)
    Data$patil_pass <- ifelse(Data$coverage >=10 & Data$beta >= 0.09, TRUE, FALSE)
    Data$sample_id <- id_mapping[id_mapping$filename==f,]$sample_id
    Data %>% filter(context3!="CN") %>% select(chr, pos, strand, methylated, unmethylated, coverage, beta, context, context2, context3, patil_pass, sample_id)
}) %>% do.call("bind_rows", .)
```

```{r, fig.width=6, fig.height=13}
meth %>% filter(!grepl("COMBINED", sample_id)) %>%
    filter(coverage > 0) %>%
    ggplot(aes(x=coverage, y=beta, colour=strand)) +
    geom_point(size=.5, alpha=.4) +
    scale_colour_manual(values=c("firebrick3", "mediumblue")) +
    geom_smooth(colour="black", method="glm", method.args=list(family="binomial"),
                linetype=2, size=.7, se=TRUE) +
    geom_smooth(method="glm", method.args=list(family="binomial"),
                linetype=1, size=.7, se=TRUE) +
    facet_wrap(sample_id~., scale="free_x", ncol=2) +
    theme_bw()

Cors <- meth %>% filter(!grepl("COMBINED", sample_id)) %>%
    filter(coverage > 0) %>%
    group_by(sample_id, strand) %>%
    summarise(corr_cov_beta=cor(x=coverage, y=beta)) %>%
    pivot_wider(names_from="strand", values_from="corr_cov_beta")
colnames(Cors) <- gsub("-", "minus_strand", gsub("\\+", "plus_strand", colnames(Cors)))
Cors <- meth %>% filter(!grepl("COMBINED", sample_id)) %>%
    filter(coverage > 0) %>%
    group_by(sample_id) %>%
    summarise(joint_strands=cor(x=coverage, y=beta)) %>%
    ungroup %>%
    left_join(Cors)
```
The following table displays the Pearson correlation coefficients for coverage
and methylation for each sample considering all positions (joint_strands) or
each strand separately (plus_strand and minus_strand).

```{r}
kbl(Cors, digits=2) %>%
    kable_paper("hover", full_width=FALSE)
```














# MeDIP data

In the supplementary material, Patil _et al._ also provide support for
methylation of mtDNA by four samples in which 5mC was immunoprecipitated and
then sequenced. They unfortuately only provide a figure of the depth of
coverage of the mtDNA chromosome, no positive or negative controls, rendering
their interpretation difficult. Hence, this experiment does not permit
estimating the absolute methylation proportion of the region (i.e., it is not
possible to estimate that a specific position/region is methylated at 40% in
the sample) nor to compare across samples.  However, regions can be compared
across, i.e., how much more a position/region is methylated with respect to
another region or to the rest of the genome.  This means that if a mtDNA sample
has uniform methylation levels (regardless of whether they are high or low),
the overall methylation estimate for the sample cannot be interpreted.


Here we have retrieved the coverage data from the images using
[WebPlotDigitizer](https://automeris.io/WebPlotDigitizer/). The following is an
example.

```{r read_medip_data, fig.width=9, fig.height=1.5}
medip_samples <- c("HepG2_Replicate_1", "HepG2_Replicate_2",
                   "Hepato_Replicate_1", "Hepato_Replicate_2")
medip <- lapply(medip_samples, function(S) {
    read_csv(paste0("./Data/medip_data/", S, '.csv'), col_types="nn",
             col_names=c("pos", "coverage")) %>%
        mutate(pos=round(pos), coverage=round(coverage), sample_id=S,
               sample_type=factor(gsub("_Replicate_.", '', S), 
                                  levels=c("HepG2", "Hepato"))) %>%
        mutate(sample_id=factor(sample_id, 
                                levels=c("HepG2_Replicate_1", "HepG2_Replicate_2",
                                         "Hepato_Replicate_1", "Hepato_Replicate_2"))) %>%
        distinct(pos, .keep_all=TRUE) %>%
        filter(pos > 0, pos < 16596) %>%
        mutate(coverage=ifelse(coverage<0, 0, coverage))
}) %>% do.call("bind_rows", .)

# Plot example
S <- "HepG2_Replicate_1"
Fig <- paste0("./Data/medip_data/", S, '.png')
# without overlay
ggdraw() + draw_image(Fig, scale=.91, x=0)
# with overalay
P <- medip %>% filter(sample_id==S) %>% ggplot(aes(x=pos, y=coverage)) +
    geom_area(colour="red", fill="red", alpha=.4) +
    scale_x_continuous(lim=c(-158,16596)) +
    scale_y_continuous(lim=c(-1450,22000)) +
    theme_void()
ggdraw() + 
    draw_image(Fig, scale=.91, x=0) +
    draw_plot(P)
```

Now we can plot the bitmap from the paper and compare it with the extracted
data. Note that the coverage (y-axis) is kept fixed in our case to highlight
sample differences.

```{r print_paper_figure, out.width="85%"}
include_graphics("./Data/medip_data/img_patil_paper.png")
```

```{r print_reverse_engineered_data, fig.width=7.36, fig.height=4.5}
medip %>% ggplot() +
    geom_area(aes(x=pos, y=coverage, color=sample_type, fill=sample_type)) +
    scale_color_manual(values=c("blue", "green")) +
    scale_fill_manual(values=c("blue", "green")) +
    facet_wrap(~sample_id, ncol=1) +
    theme_bw() +
    theme(legend.position="left")
```

Now that we have the meDIP coverage data, we can see if correlates at all with
the methylation based on BS-seq. Only the biological conditions that were
analyzed with MeDIP-seq are compared in the BS-seq data (HepG2 and
hepatocytes). Positions with less than 10x coverage are discarded from the
analysis to avoid noisy observations; only positions that are present in all
samples are used to calculate the correlations. 

MeDIP-seq methylation levels are only relative and are estimated based on the
coverage from the supplementary figure from the original publication.

```{r interpolate_missing_medip}
# clean and interpolate missing data
medip_interp <- lapply(unique(medip$sample_id), function(S){
    Data <- filter(medip, sample_id==S)
    Spline <- as_tibble(spline(Data$pos, Data$coverage, xout=1:16596))
    names(Spline) <- c("pos", "cov_spline")
    left_join(Spline, select(Data, pos, coverage), by="pos") %>%
        mutate(sample_id=S)
}) %>% do.call("bind_rows", .) %>%
    mutate(beta=round(ifelse(is.na(coverage), cov_spline, coverage))) %>%
    mutate(sample_id=as.character(sample_id)) %>%
    mutate(pos=as.integer(pos)) %>%
    select(sample_id, pos, beta)
```

```{r correlate_bs_medip, fig.width=7, fig.height=5}
Data <- bind_rows(
    meth %>% filter(sample_id %in% c("HEPATOCYTES", "HEPG2_Lib1_S1",
                                     "HEPG2_Lib2_S2", "HepG2_COMBINED"), coverage>=10) %>%
    select(sample_id, pos, beta),
    medip_interp) %>%
    mutate(method=case_when(
                    sample_id %in% c("HEPG2_Lib1_S1", "HEPG2_Lib2_S2", 
                                     "HepG2_COMBINED", "HEPATOCYTES") ~ "BS-seq",
                    sample_id %in% c("HepG2_Replicate_1", "HepG2_Replicate_2", 
                                     "Hepato_Replicate_1", "Hepato_Replicate_2") ~ "MeDIP-seq"),
           sample_type=case_when(
                    sample_id %in% c("HEPG2_Lib1_S1", "HEPG2_Lib2_S2", "HepG2_COMBINED",
                                     "HepG2_Replicate_1", "HepG2_Replicate_2") ~ "HepG2",
                    sample_id %in% c("HEPATOCYTES", "Hepato_Replicate_1",
                                     "Hepato_Replicate_2") ~ "Hepatocytes"))

# restrict to intersecting positions
common_positions <- Reduce("intersect",split(Data$pos, Data$sample_id))
Data <- Data %>% filter(pos %in% common_positions)

# calculate spearman correlations
correlations <- lapply(unique(Data$sample_id), function(id1){
    lapply(unique(Data$sample_id), function(id2){
        tmpData <- inner_join(Data %>% filter(sample_id==id1), Data %>% filter(sample_id==id2), by="pos")
        cor.test(x=tmpData$beta.x, y=tmpData$beta.y, method="spearman") %>% tidy %>%
            mutate(id1=id1, id2=id2, method1=tmpData$method.x[1], method2=tmpData$method.y[1], 
                   sample_type1=tmpData$sample_type.x[1], sample_type2=tmpData$sample_type.y[1]) %>%
            select(id1, sample_type1, method1, id2, sample_type2, method2, rho=estimate, p.value)
    }) %>% do.call("bind_rows", .)
}) %>% do.call("bind_rows", .)

mat.cor <- correlations %>% select(id1, id2, rho) %>%
    pivot_wider(names_from="id2", values_from="rho") %>%
    as.data.frame
rownames(mat.cor) <- mat.cor$id1
mat.cor <- as.matrix(mat.cor[,-1])
#all(rownames(mat.cor)==colnames(mat.cor))
diag(mat.cor) <- NA

annot_df <- correlations %>% select(sample_id=id1, sample_type=sample_type1, method=method1) %>%
    distinct
annot_df <- as.data.frame(annot_df[match(annot_df$sample_id, rownames(mat.cor)),])
rownames(annot_df) <- annot_df$sample_id
annot_df <- annot_df[,-1]
#all(annot_df$sample_id==rownames(mat.cor))
#all(rownames(annot_df)==rownames(mat.cor))

pheatmap(mat.cor,
         cluster_rows=FALSE, cluster_cols=FALSE,
         annotation_row=annot_df,
         annotation_col=annot_df,
         display_numbers=TRUE)
```

The heatmap above represents the pairwise Spearman correlation coefficients
between all the HepG2 and hepatocyte samples, analysed using BS-seq and
MeDIP-seq. (i) Samples cluster strongly according to the method used (BS- and
MeDIP-seq) rather than according to the biological condition. (ii) Even within
the same analysis method, samples cluster with different biological conditions
rather than with the same type of tissue (highest correlation between BS-seq samples
HepG2_COMIBINED ~ HEPATOCYTES: rho=0.95; highest correlation in MeDIP-seq samples:
HepG2_Replicate_1 ~ Hepato_Replicate_2: rho=0.98).




# Appendix

## M-bias files before trimming

```{r}
id_mapping %>%
    mutate(Data=paste0("[", sample_id, "](./Data/mbias_1/", filename, "_R1_bismark_bt2_pe.M-bias.txt)"),
           Plot_R1=paste0("[", sample_id, "](./Data/mbias_1/pngs/", filename, "_R1_bismark_bt2_pe.M-bias_R1.png)"),
           Plot_R2=paste0("[", sample_id, "](./Data/mbias_1/pngs/", filename, "_R1_bismark_bt2_pe.M-bias_R2.png)")) %>%
    select(Sample=sample_id, Data, Plot_R1, Plot_R2) %>%
    kbl() %>% kable_paper("hover", full_width=FALSE)
```

## M-bias files after trimming
```{r}
id_mapping %>%
    mutate(Data=paste0("[", sample_id, "](./Data/mbias_2/", filename, "_trimmed_R1_bismark_bt2_pe.M-bias.txt)"),
           Plot_R1=paste0("[", sample_id, "](./Data/mbias_2/pngs/", filename, "_trimmed_R1_bismark_bt2_pe.M-bias_R1.png)"),
           Plot_R2=paste0("[", sample_id, "](./Data/mbias_2/pngs/", filename, "_trimmed_R1_bismark_bt2_pe.M-bias_R2.png)")) %>%
    select(Sample=sample_id, Data, Plot_R1, Plot_R2) %>%
    kbl() %>% kable_paper("hover", full_width=FALSE)
```


